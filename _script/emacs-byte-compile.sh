#!/bin/sh
cd $HOME/.emacs.d && \
    emacs --batch -f batch-byte-compile obsolete-emacs28-fix.el && \
    emacs --batch -f batch-byte-compile init.el
