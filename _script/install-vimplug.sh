#!/bin/sh
echo 'Do you want to install vimplug for [V]im or [N]eovim?'
read -p '> ' TARGET

case "$TARGET" in
    "V" | "v" ) curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim ;;
    "N" | "n" ) curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim ;;
    * ) echo "Bad input"; exit 1 ;;
esac
