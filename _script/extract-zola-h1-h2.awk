#!/bin/awk -f

match($0, "<h1 id=\"(.*)\">(.*)</h1>", m) { printf "- [%s](#%s)\n", m[2], m[1] }
match($0, "<h2 id=\"(.*)\">(.*)</h2>", m) { printf "  + [%s](#%s)\n", m[2], m[1] }
