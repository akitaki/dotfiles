;; Akitaki <robinhuang123@gmail.com>

;; Leaf keywords
(eval-and-compile
  (setq gc-cons-threshold 100000000) ;; 100MB
  ;; Use native compilation
  (setq comp-deferred-compilation t)

  (load-file (concat user-emacs-directory "obsolete-emacs28-fix.elc"))
  (customize-set-variable
   'package-archives '(("gnu"   . "https://elpa.gnu.org/packages/")
		       ("melpa" . "https://melpa.org/packages/")
		       ("org"   . "https://orgmode.org/elpa/")))
  (package-initialize)

  ;; Leaf
  (unless (package-installed-p 'leaf)
    (package-refresh-contents)
    (package-install 'leaf))

  (leaf leaf-keywords
    :ensure t
    :config
    (leaf el-get :ensure t)
    (leaf-keywords-init)))

;; Packages

;; leaf.el itself
(leaf leaf
  :config
  (leaf leaf-convert :ensure t)
  (leaf leaf-tree
    :ensure t
    :custom ((imenu-list-size . 30)
	     (imenu-list-position . 'left))))

;; Dispose customize output to dummy file
(leaf cus-edit
  :custom `((custom-file . ,(locate-user-emacs-file "custom.el"))))

(eval-and-compile
  (leaf *evil
    :config
    ;; Needed for evil
    ;; TODO: Revisit this setting when Emacs 28 comes
    (leaf goto-chg :ensure t)
    (leaf undo-tree :ensure t :init (global-undo-tree-mode))

    (leaf evil
      :ensure t
      :custom (;; For evil-collection
	       (evil-want-keybinding . nil)
	       ;; For `cgn` command
	       (evil-search-module . 'evil-search)
	       ;; For evil undo
	       (evil-undo-system . 'undo-tree))
      :global-minor-mode t
      :config (define-key evil-motion-state-map ";" 'evil-ex)
      (evil-define-key 'normal 'global
	"]c" 'diff-hl-next-hunk
	"[c" 'diff-hl-previous-hunk
	"\\hu" 'diff-hl-revert-hunk
	"\\;" "A;"
	"K" 'lsp-ui-doc-glance))

    (leaf evil-collection
      :ensure t
      :after evil
      :config
      (evil-collection-init))
    (leaf evil-surround
      :ensure t
      :after evil
      :config
      (global-evil-surround-mode 1))
    (leaf evil-nerd-commenter
      :ensure t
      :after evil
      :config
      (evilnc-default-hotkeys))))


(leaf *gui
  :config
  ;; Map bold texts to semibold to avoid distraction
  (defun replace-bold-with-semibold ()
    (mapc
     (lambda (face)
       (when (eq (face-attribute face :weight) 'bold)
	 (set-face-attribute face nil :weight 'semibold)))
     (face-list)))

  (leaf doom-themes
    :ensure t
    :custom ((doom-themes-enable-bold . t)
	     (doom-themes-treemacs-theme . "doom-colors")
	     (doom-one-brighter-comments . t)
	     (doom-one-comment-bg . nil))
    :config
    ;; Light
    ;; (load-theme 'doom-one-light t)
    ;; (load-theme 'doom-solarized-light t)

    ;; Dark
    (load-theme 'doom-one t)
    (doom-themes-visual-bell-config)
    (replace-bold-with-semibold))

  (leaf doom-modeline
    :ensure t
    :global-minor-mode t)
  
  (leaf centaur-tabs
    :ensure t
    :init
    ;; Underline the selected tab
    (setq x-underline-at-descent-line t
	  centaur-tabs-style "bar"
	  centaur-tabs-set-bar 'under)
    :bind (("C-<prior>" . centaur-tabs-backward-tab)
	   ("C-<next>" . centaur-tabs-forward-tab)
	   ("C-<tab>" . centaur-tabs-forward-tab)
	   ("C-<iso-lefttab>" . centaur-tabs-forward-group))
    :custom ((centaur-tabs-height . 36)
	     (centaur-tabs-set-icons . t)
	     (centaur-tabs-set-modified-marker . t)
	     (centaur-tabs-show-navigation-buttons . t)
	     (centaur-tabs-cycle-scope . 'tabs)
	     (centaur-tabs-group-by-projectile-project))
    :global-minor-mode t)

  ;; Fuzzy jump to character
  (leaf avy :ensure t
    :bind (("C-:" . avy-goto-char)))

  ;; Asynchronous relative line numbers
  (leaf nlinum-relative
    :ensure t
    :init
    (add-hook 'prog-mode-hook 'nlinum-relative-mode)
    :commands nlinum-relative-mode
    :config
    (nlinum-relative-setup-evil)
    :custom-face
    (linum . '((t (:inherit default :foreground "dim gray" :strike-through nil :underline nil :slant normal :weight normal)))))

  ;; Ligatures
  (leaf ligature
    :el-get mickeynp/ligature.el
    :require t
    :init
    ;; Fira Code specific setup
    ;; Taken from <https://github.com/mickeynp/ligature.el/issues/8>
    (ligature-set-ligatures 'prog-mode '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
					 ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
					 "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
					 "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
					 "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
					 "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
					 "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
					 "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
					 "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
					 "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%"))
    (global-ligature-mode 't))

  ;; Sidebar
  (leaf treemacs-evil
    :ensure t
    :require t
    :after treemacs)
  (leaf treemacs-projectile
    :ensure t
    :require t
    :after treemacs)
  (leaf treemacs
    :ensure t
    :commands treemacs treemacs-select-window
    :bind (("M-0" . treemacs-select-window))
    :config
    (doom-themes-treemacs-config))

  (leaf diff-hl
    :ensure t
    :after evil
    ;; Defer loading after prog-mode hook
    :init
    (add-hook 'prog-mode-hook 'diff-hl--global-turn-on)
    :commands diff-hl--global-turn-on

    :config
    (setq diff-hl-loaded t)
    ;; Integrate with magit
    (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
    (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
    ;; Enable globally
    (global-diff-hl-mode))

  ;; Needed for centaur-tabs
  (leaf all-the-icons :ensure t)

  ;; Welcome buffer
  (leaf dashboard
    :ensure t
    :custom
    ((dashboard-startup-banner . 'logo))
    :config
    (dashboard-setup-startup-hook)))

(leaf *lsp
  :config
  (leaf flycheck
    :ensure t)
  (leaf flycheck-pos-tip
    :ensure t
    :config
    (flycheck-pos-tip-mode))
  (leaf clang-format
    :ensure t)
  (leaf yasnippet
    :ensure t
    :config
    (yas-global-mode 1))
  (leaf lsp-ui
    :ensure t
    :after lsp-mode
    :custom
    (lsp-ui-doc-position . 'at-point)
    (lsp-ui-doc-enable . nil)
    (lsp-ui-sideline-enable . nil))
  (leaf lsp-ivy
    :ensure t
    :after lsp-mode
    :config
    (evil-define-key 'normal 'global
	"\\s" 'lsp-ivy-workspace-symbol))
  (leaf company
    :ensure t
    :defer-config
    (setq lsp-enable-snippet t)
    :custom
    ;; Make company-mode snappier, following the instructions from lsp-mode
    (company-minimum-prefix-length . 1)
    (company-idle-delay . 0.0))
  (leaf lsp-treemacs
    :ensure t
    :after treemacs lsp
    :custom
    ;; Put the lsp symbols window on right-hand side
    (lsp-treemacs-symbols-position-params . `((side . right) (slot . 1) (window-width . 35))))
  (leaf lsp-dart
    :ensure t)
  (leaf lsp-pyright
    :ensure t)
  (leaf lsp-mode
    :ensure t
    :require all-the-icons
    :commands lsp
    :custom
    ;; Was about to use "s-l", but it currently conflicts with i3wm
    (lsp-keymap-prefix . "C-l")
    (lsp-clients-clangd-args . '("--header-insertion=never"))
    (lsp-signature-doc-lines . 2)
    :config
    (replace-bold-with-semibold)
    ;; CMake LSP; To install server run `pip3 install cmake-language-server --user`
    (lsp-register-client
     (make-lsp-client :new-connection (lsp-stdio-connection "cmake-language-server")
		      :activation-fn (lsp-activate-on "cmake")
		      :server-id 'cmake))
    (add-hook 'js-mode-hook #'lsp)
    (add-hook 'c-mode-hook #'lsp)
    (add-hook 'c++-mode-hook #'lsp)
    (add-hook 'cmake-mode-hook #'lsp)))

;; Assistive packages
(leaf *misc
  :config
  (leaf hl-todo
    :ensure t
    :init
    (add-hook 'prog-mode-hook (hl-todo-mode)))
  (leaf editorconfig
    :ensure t
    :global-minor-mode t)
  ;; For ivy history
  (leaf ivy-prescient
    :ensure t
    :global-minor-mode t
    :init
    ;; Fix error on lsp-ivy-workspace-symbol
    ;; As per https://github.com/emacs-lsp/lsp-ivy/issues/18
    (setq ivy-prescient-sort-commands '(:not swiper
					     swiper-isearch
					     counsel-imenu
					     ivy-switch-buffer
					     lsp-ivy-workspace-symbol
					     ivy-resume
					     ivy--restore-session
					     counsel-switch-buffer)))
  (leaf ivy
    :ensure t
    :global-minor-mode t)
  (leaf which-key
    :ensure t
    :global-minor-mode t)
  (leaf projectile
    :ensure t
    :global-minor-mode t
    :config
    (define-key projectile-mode-map
      (kbd "C-c p") 'projectile-command-map)))

;; Language-specific modes that are missing from default Emacs
(leaf *langs
  :config
  (leaf adoc-mode
    :ensure t)
  (leaf cmake-mode
    :ensure t)
  (leaf rust-mode
    :ensure t)
  (leaf dart-mode
    :ensure t)
  (leaf yaml-mode
    :ensure t
    :config
    (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))
  (leaf dockerfile-mode
    :ensure t))

;; Full-fledged functionalities
(leaf *apps
  :config
  (leaf magit
    :ensure t
    :commands magit magit-status
    :custom
    ;; Hide "^M" from the magit diff display
    (magit-diff-hide-trailing-cr-characters . t)))

(leaf *builtins
  :config
  ;; Declutter the UI
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (toggle-scroll-bar -1)
  ;; Auto-close pair and highlight matching parenthesis
  (electric-pair-mode 1)
  (show-paren-mode 1)
  ;; For LSP to run smooth, as suggested by M-x lsp-doctor
  (setq gc-cons-threshold 100000000) ;; 100MB
  (setq read-process-output-max (* 1024 1024)) ;; 1MB
  ;; Don't use tabs
  (setq indent-tabs-mode nil)
  ;; Always focus on the help window
  (setq help-window-select t)
  ;; Underline and make URLs clickable
  (global-goto-address-mode)
  ;; Disable scroll bars for new frames
  ;; As per https://emacs.stackexchange.com/a/23785
  (defun disable-scroll-bars (frame)
    (modify-frame-parameters frame
			     '((vertical-scroll-bars . nil)
			       (horizontal-scroll-bars . nil))))
  (add-hook 'after-make-frame-functions 'disable-scroll-bars)
  ;; Default font
  (if (< (sqrt
	  (+ (expt (x-display-mm-width) 2)
	     (expt (x-display-mm-height) 2)))
	 500)
      (add-to-list 'default-frame-alist
		   '(font . "Fira Code-13"))
    (add-to-list 'default-frame-alist
		 '(font . "Fira Code-12")))
  ;; Use ibuffer for buffer management
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  ;; Move backup files to somewhere else
  (setq backup-directory-alist `(("." . "~/.cache/")))
  ;; Start server if not yet
  (load "server")
  (unless (server-running-p) (server-start)))

(leaf *lsptramp
  :config
  ;; Fix LSP over TRAMP
  ;; As per https://github.com/emacs-lsp/lsp-mode/issues/2514#issuecomment-759452037
  (defun start-file-process-shell-command@around (start-file-process-shell-command name buffer &rest args)
    "Start a program in a subprocess.  Return the process object for it. Similar to `start-process-shell-command', but calls `start-file-process'."
    (let ((command (mapconcat 'identity args " ")))
      (funcall start-file-process-shell-command name buffer command)))

  (advice-add 'start-file-process-shell-command :around #'start-file-process-shell-command@around)

  ;; Use remote .profile to setup PATH for tramp
  ;; As per suggestion in https://stackoverflow.com/a/61169654
  (require 'tramp)
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)

  (with-eval-after-load "lsp-rust"
    (lsp-register-client
     (make-lsp-client
      :new-connection (lsp-tramp-connection "rust-analyzer")
      :remote? t
      :major-modes '(rust-mode rustic-mode)
      :initialization-options 'lsp-rust-analyzer--make-init-options
      :notification-handlers (ht<-alist lsp-rust-notification-handlers)
      ;; Bugged
      ;; :action-handlers (ht ("rust-analyzer.runSingle" #'lsp-rust--analyzer-run-single))
      :library-folders-fn (lambda (_workspace) lsp-rust-library-directories)
      :after-open-fn (lambda ()
		       (when lsp-rust-analyzer-server-display-inlay-hints
			 (lsp-rust-analyzer-inlay-hints-mode)))
      :ignore-messages nil
      :server-id 'rust-analyzer-remote))))

(provide 'init)
;;; init.el ends here
