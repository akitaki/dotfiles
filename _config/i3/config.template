# vim: ft=conf:

font pango:Fira Code $TEMPLATE_WM_FONT_SIZE

# Basic keys
set $mod Mod4
set $up l
set $down k
set $left j
set $right semicolon

# Make urgent windows always steal focus
focus_on_window_activation focus

# Move / resize floatings while holding MOD
floating_modifier $mod

# i3-gaps only gaps config
smart_borders on
default_border none
gaps top $TEMPLATE_BAR_HEIGHT
gaps inner 5
bindsym $mod+g exec --no-startup-id "if [ `i3-msg -t get_tree | grep -Po \
    '.*\\"gaps\\":{\\"inner\\":\K(-|)[0-9]+(?=.*\\"focused\\":true)'` -eq 0 ]; then \
        i3-msg gaps inner current set 0; \
    else \
        i3-msg gaps inner current set 5; \
    fi"

################
# Startup jobs #
################
exec_always --no-startup-id polybar main
exec_always --no-startup-id i3-smart-gap
exec_always --no-startup-id fcitx&
exec_always --no-startup-id picom --experimental-backends
exec_always --no-startup-id feh --bg-fill "/home/akitaki/.config/wallpaper/current"
exec_always --no-startup-id setxkbmap -option altwin:swap_alt_win

################
#   Bindings   #
################
# Pulseaudio volume via media keys
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym XF86AudioMute        exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioMicMute     exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle

# Xbacklight via media keys
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 5 && notify-send -t 1500 "Backlight" "$(xbacklight -get)"
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 5 && notify-send -t 1500 "Backlight" "$(xbacklight -get)"

# CMUS control via media keys
bindsym XF86AudioPlay exec --no-startup-id cmus-remote --pause

# Rename workspace
bindsym $mod+n exec ~/.script/rename-workspace.sh

# Screenshots using scrot
#  MOD + p : fullscreen
#  MOD + o : current window
#  MOD + i : manual selection
bindsym $mod+p exec scrot -f /tmp/$(date +"%Y-%m-%d-%H:%M:%S-%3N").png
bindsym $mod+o exec scrot -u -f /tmp/$(date +"%Y-%m-%d-%H:%M:%S-%3N").png
bindsym --release $mod+i exec scrot -f /tmp/$(date +"%Y-%m-%d-%H:%M:%S-%3N").png -s

# Terminal
bindsym $mod+Return exec --no-startup-id alacritty

# Rofi combo menu
bindsym $mod+d exec --no-startup-id "rofi -show drun -modi drun,window,run -show-icons"

# Close window
bindsym $mod+Shift+q kill

# Enter resize mode
bindsym $mod+r mode "resize"

################
#  Navigation  #
################
# Elevate / descend focus to parent / child
bindsym $mod+a focus parent
bindsym $mod+c focus child

# Other normal bindings
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+h split h
bindsym $mod+v split v

bindsym $mod+f fullscreen toggle

# Layout switching
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Floating toggles
bindsym $mod+Shift+space floating toggle
bindsym $mod+space focus mode_toggle

# Scratchpad management
bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

# Sticky toggle
bindsym $mod+Shift+s sticky toggle

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

bindsym $mod+Shift+c reload
bindsym $mod+Shift+r restart
bindsym $mod+Shift+e exit

################
# Resize Mode  #
################
mode "resize" {
    bindsym $left  resize shrink width  10 px or 10 ppt
    bindsym $down  resize grow   height 5  px or 5  ppt
    bindsym $up    resize shrink height 5  px or 5  ppt
    bindsym $right resize grow   width  5  px or 5  ppt

    bindsym Left   resize shrink width  5  px or 5  ppt
    bindsym Down   resize grow   height 5  px or 5  ppt
    bindsym Up     resize shrink height 5  px or 5  ppt
    bindsym Right  resize grow   width  5  px or 5  ppt

    bindsym Return mode   "default"
    bindsym Escape mode   "default"
    bindsym $mod+r mode   "default"
}

################
#    Theme     #
################
# class                 border  bground text    indicator child_border
client.focused          #3D4C51 #3D4C51 #F8F8F2 #3D4C51   #3D4C51
client.focused_inactive #44475A #44475A #F8F8F2 #44475A   #44475A
client.unfocused        #282A36 #282A36 #BFBFBF #282A36   #282A36
client.urgent           #44475A #FF5555 #F8F8F2 #FF5555   #FF5555
client.placeholder      #282A36 #282A36 #F8F8F2 #282A36   #282A36
client.background       #F8F8F2

for_window [window_role="GtkFileChooserDialog"] resize set 1200 800, move position center
for_window [class="^Pavucontrol$"] floating enable
