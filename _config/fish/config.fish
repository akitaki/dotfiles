if status --is-interactive
    bind \cn "commandline -r 'ranger'; commandline -f repaint; commandline -f execute"
end
set -x PATH ~/.cargo/bin/ $PATH
