; vim: ft=dosini:

[colors]
background = #aa222222
background-alt = #aa353535
foreground = #f2f6fc
foreground-alt = #d3dae6
primary = #ebcb8b
secondary = #a3be8c
alert = #bf616a

b1 = #2e3440
b2 = #3b4252
b3 = #434c5e
b4 = #4c566a

w3 = #d8dee9
w2 = #e5e9f0
w1 = #eceff4

a4 = #8fbcbb
a3 = #88c0d0
a2 = #81a1c1
a1 = #5e81ac

nred = #bf616a
norg = #d08770
nylw = #ebcb8b
ngrn = #a3be8c
nppl = #b48ead

[bar/main]
width = 100%
height = $TEMPLATE_BAR_HEIGHT
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = ${colors.nred}

underline-size = 2

border-bottom-size = 1
border-bottom-color = #e8222222

padding-left = 0
padding-right = 2

wm-restack = i3
override-redirect = true

module-margin-left = 1
module-margin-right = 0.5

; To get the material icons font on Gentoo, use `myrvolay` overlay
font-0 = Fira Mono:size=$TEMPLATE_BAR_FONT_SIZE;$TEMPLATE_BAR_FONT_OFFSET
font-1 = Material Icons:size=$TEMPLATE_BAR_ICON_SIZE;$TEMPLATE_BAR_ICON_OFFSET
font-2 = Noto Sans Mono CJK JP:size=$TEMPLATE_BAR_FONT_SIZE;$TEMPLATE_BAR_FONT_OFFSET

modules-left = menubutton titlemain i3
modules-center = cmus
modules-right = pulseaudio wlan filesystem memory cpu eth battery temperature date

tray-position = right
tray-padding = 2

cursor-click = pointer
cursor-scroll = ns-resize

[bar/extend]
monitor = HDMI-A-0

width = 100%
height = $TEMPLATE_BAR_HEIGHT
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = ${colors.nred}

underline-size = 2

border-bottom-size = 1
border-bottom-color = #e8222222

padding-left = 0
padding-right = 2

wm-restack = i3
override-redirect = true

module-margin-left = 1
module-margin-right = 0.5

; To get the material icons font on Gentoo, use `myrvolay` overlay
font-0 = Fira Mono:size=$TEMPLATE_BAR_FONT_SIZE;$TEMPLATE_BAR_FONT_OFFSET
font-1 = Material Icons:size=$TEMPLATE_BAR_ICON_SIZE;$TEMPLATE_BAR_ICON_OFFSET
font-2 = Noto Sans Mono CJK JP:size=$TEMPLATE_BAR_FONT_SIZE;$TEMPLATE_BAR_FONT_OFFSET

modules-left = menubutton title i3
modules-center = cmus
modules-right = pulseaudio wlan filesystem memory cpu eth battery temperature date

tray-position = right
tray-padding = 2

cursor-click = pointer

[module/cmus]
type = custom/script

exec = ~/.config/polybar/cmus.sh
exec-if = pgrep -x cmus
interval = 1

click-left = cmus-remote --next
click-right = cmus-remote --prev
click-middle = cmus-remote --pause
scroll-up = cmus-remote --volume +5%
scroll-down = cmus-remote --volume -5%

label-font = 3
format = <label>
format-underline = ${colors.foreground-alt}
label = %output%
label-maxlen = 50

[module/filesystem]
type = internal/fs
interval = 30

mount-0 = /
format-mounted = <ramp-capacity> <label-mounted>
; format-mounted-underline = ${colors.ngrn}
label-mounted = %free%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

format-mounted-foreground = ${colors.foreground-alt}
label-mounted-foreground = ${colors.foreground}

ramp-capacity-0 = ▁
ramp-capacity-1 = ▂
ramp-capacity-2 = ▃
ramp-capacity-3 = ▄
ramp-capacity-4 = ▅
ramp-capacity-5 = ▆
ramp-capacity-6 = ▇
ramp-capacity-7 = █

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %name%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.ngrn}
label-focused-padding = 1

; unfocused = Inactive workspace on any monitor
label-unfocused = %name%
label-unfocused-padding = 1

; visible = Active workspace on unfocused monitor
label-visible = %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %name%
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

; Separator in between workspaces
; label-separator = |

[module/cpu]
type = internal/cpu
interval = 2
format = <ramp-load> <label>
format-prefix-foreground = ${colors.foreground-alt}
; format-underline = ${colors.ngrn}
label = %percentage:02%%
format-foreground = ${colors.foreground-alt}
label-foreground = ${colors.foreground}

ramp-load-0 = ▁
ramp-load-1 = ▂
ramp-load-2 = ▃
ramp-load-3 = ▄
ramp-load-4 = ▅
ramp-load-5 = ▆
ramp-load-6 = ▇
ramp-load-7 = █

[module/memory]
type = internal/memory
interval = 2
format = <ramp-used> <label>
format-prefix-foreground = ${colors.foreground-alt}
label = %gb_used%
format-foreground = ${colors.foreground-alt}
label-foreground = ${colors.foreground}

ramp-used-0 = ▁
ramp-used-1 = ▂
ramp-used-2 = ▃
ramp-used-3 = ▄
ramp-used-4 = ▅
ramp-used-5 = ▆
ramp-used-6 = ▇
ramp-used-7 = █

[module/wlan]
type = internal/network
interface = wlp3s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
label-connected = %{A1:wpa_cli select_network "$(wpa_cli list_networks | tail -n +3 | rofi -dmenu | cut -d' ' -f1)""')":}%{A3:wpa_cli disconnect:}%signal%%{A}%{A}

format-disconnected = <label-disconnected>
label-disconnected = %{A1:wpa_cli select_network "$(wpa_cli list_networks | tail -n +3 | rofi -dmenu | cut -d' ' -f1)""')":}%{A}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = !
ramp-signal-4 = !
ramp-signal-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = enp0s31f6
interval = 3.0

format-connected-underline = ${colors.a3}
format-connected-prefix = "E "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 1

date =
date-alt = "%Y-%m-%d"

time = %a %H:%M
time-alt = " %H:%M:%S"

format-prefix-foreground = ${colors.foreground-alt}
; format-underline = ${colors.a3}

label = %date%%time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume>
format-volume-foreground = ${colors.foreground-alt}
; format-volume-underline = ${colors.a3}
label-volume =  %percentage%%
label-volume-foreground = ${colors.foreground}

label-muted = 
label-muted-foreground = ${colors.foreground}

click-right = "pavucontrol"

[module/alsa]
type = internal/alsa

master-mixer = PCM

format-volume = V <label-volume>%
format-volume-foreground = ${colors.foreground-alt}
format-volume-underline = ${colors.a3}
label-volume = %percentage%%
label-volume-foreground = ${colors.foreground}

label-muted = mute
label-muted-foreground = #999

[module/battery]
type = internal/battery
full-at = 99
battery = BAT0
adapter = ADP1
time-format = %-H:%M

format-discharging-foreground = ${colors.foreground-alt}
format-discharging =  <label-discharging>
label-discharging-foreground = ${colors.foreground}
label-discharging = %percentage%% (%time%)

format-charging =  <label-charging>
label-charging = %percentage%%

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-warn = <ramp> <label-warn>

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

[module/menubutton]
type = custom/script
exec = echo ' '
# Open apps menu by click
click-left = "rofi -show drun -modi drun,window,run -show-icons"
# Open new workspace by mid-click
# Source: <https://faq.i3wm.org/question/6004/creating-a-new-workspace.1.html>
click-middle = "i3-msg workspace $(($(i3-msg -t get_workspaces | tr , '\n' | grep '"num":' | cut -d : -f 2 | sort -rn | head -1) + 1))"
# Close window by right-click
click-right = "i3-msg kill"
interval = 600

# Main title module also handles the gaps (-g)
[module/titlemain]
type = custom/script
exec = i3-smart-gap -g
tail = true
interval = 0

# External monitor title module doesn't handle the gaps (no -g)
[module/title]
type = custom/script
exec = i3-smart-gap
tail = true
interval = 0


[settings]
screenchange-reload = true

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
